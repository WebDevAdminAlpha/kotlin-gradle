package security.sample.helper

import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.io.Serializable
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

class JacksonHelper : Serializable {
    val mapper = jacksonObjectMapper()

    @Throws(Exception::class)
    fun convertJson(JSON: String){
        val obj = mapper.readValue(JSON, Bean::class.java)
    }

    internal class Bean {
        @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
        var obj: Any? = null
    }

}